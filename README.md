## *Millerozyma farinosa* CBS 7064

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEA73835](https://www.ebi.ac.uk/ena/browser/view/PRJEA73835)
* **Assembly accession**: [GCA_000315895.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000315895.1)
* **Original submitter**: Genolevures Consortium

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: ASM31589v1
* **Assembly length**: 21,459,642
* **#Chromosomes**: 14
* **Mitochondiral**: No 
* **N50 (L50)**: 1,666,063 (6)

### Annotation overview

* **Original annotator**: Genolevures Consortium
* **CDS count**: 11175
* **Pseudogene count**: 77
* **tRNA count**: 288
* **rRNA count**: 16
* **Mobile element count**: 0
