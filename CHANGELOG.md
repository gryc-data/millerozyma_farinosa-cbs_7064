# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.1 (2021-05-17)

### Edited

* Build feature hierarchy.

## v1.0 (2021-05-17)

### Added

* The 14 annotated chromosomes of Millerozyma farinosa CBS 7064 (source EBI, [GCA_000315895.1](https://www.ebi.ac.uk/ena/browser/view/GCA_000315895.1)).
